# have a list of fruits

fruit_list = ["banana", "peach", "kiwi", "cherry", "cherry", "apricot"]

print(f"The list is:  {str(fruit_list)}")

# remove duplicated from list
result = []
for i in fruit_list:
    if i not in result:
        result.append(i)
# sort list of fruits
# printing list after removal and sorting
print(f"The list after removing duplicates:  {str(result)}")


# sort list of fruits
def sorting(li):
    for i in range(len(li)):
        for j in range(len(li)):
            if li[i] < li[j]:
                li[j], li[i] = li[i], li[j]
    return li


listToSort = result
print(f"The list after sorting: {sorting(listToSort)}")
